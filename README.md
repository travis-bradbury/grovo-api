# Grovo API

This a PHP connector to https://grovo.readme.io/docs/user-management-api.

## Usage

Create or update a user with an ID, email, first name, and last name.
```php
use \acromedia\Grovo\User;
use \acromedia\Grovo\Connector;
use \acromedia\Grovo\OnboardingOption\SSO;

$users[] = User::create('1234', 'joe.smith@example.com', 'Joe', 'Smith');
$users[] = User::create('5678', 'jane.doe@example.com', 'Jane', 'Doe');

$grovo = Connector::create(SSO, 'grovo.example.com', '123abc');
$response = $grovo->send(...$users);

if (!$response->successful()) {
  foreach ($response->getErrors() as $error) {
    log($error->getMessage();
  }
}
```

Grovo supports partial updates for users. Fields not specified are left
unchanged.
```php
use \acromedia\Grovo\User;
use \acromedia\Grovo\Connector;
use \acromedia\Grovo\OnboardingOption\SSO;

$joe = User::createPartial('1234')->inactive();

$grovo = Connector::create(SSO, 'grovo.example.com', '123abc');
$grovo->send(...[$joe]);
```
