<?php

namespace acromedia\Grovo\tests;

use acromedia\Grovo\Connector;
use acromedia\Grovo\OnboardingOption\SSO;
use acromedia\Grovo\Response\BadRequest;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;

/**
 * Test response classes.
 */
final class ResponseTest extends GrovoTestBase
{

    /**
     * Ensure the connector returns appropriate responses and errors.
     */
    public function testResponse()
    {
        $users = $this->getUsers();

        // Ensure 2xx responses have the correct response.
        $client = $this->createMock(ClientInterface::class);
        $client->method('request')
          ->willReturn(new Response(202, [], ''));
        $connector = new Connector($client, new SSO(), 'grovo.example.com/api', '123456');
        $response = $connector->send(...$users);
        $this->assertTrue($response->successful(), 'Successful response returned.');

        // 403 errors return a Forbidden response.
        $exception = $this->getClientException(403);
        $exception->method('getResponse')
          ->willReturn(new Response(403, [], '{"message":"Forbidden"}'));
        $client = $this->createMock(ClientInterface::class);
        $client->method('request')
          ->willThrowException($exception);
        $connector = new Connector($client, new SSO(), 'grovo.example.com/api', '123456');
        $response = $connector->send(...$users);
        $this->assertFalse($response->successful(), '403 error is an unsuccessful response.');
        $this->assertSame($response->getErrors()[0]->getMessage(), 'Forbidden. Did you use the correct API key?', '403 responses return a Forbidden response.');

        // Errors from Grovo create Error objects.
        $exception = $this->getClientException(400);
        $exception->method('getResponse')
          ->willReturn(new Response(400, [], '{"errors":[{"title":"EmployeeId Missing"},{"title":"Some other error"}]}'));
        $client = $this->createMock(ClientInterface::class);
        $client->method('request')
          ->willThrowException($exception);
        $connector = new Connector($client, new SSO(), 'grovo.example.com/api', '123456');
        $response = $connector->send(...$users);
        $this->assertInstanceOf(BadRequest::class, $response, 'Bad Request returned');
        $this->assertFalse($response->successful(), '400 error is an unsuccessful response.');
        $this->assertSame($response->getErrors()[0]->getMessage(), 'EmployeeId Missing', 'Received expected message for error 1');
        $this->assertSame($response->getErrors()[1]->getMessage(), 'Some other error', 'Received expected message for error 2');
    }

    /**
     * Get an HTTP client exception with a particular error code.
     *
     * PhpUnit can not mock final methods, but use reflection to hack around
     * that.
     *
     * See https://phpunit.readthedocs.io/en/7.3/test-doubles.html
     * See https://stackoverflow.com/a/33600588
     *
     * @param int $code
     *   A status code for the HTTP client exception.
     *
     * @return \GuzzleHttp\Exception\ClientException
     *   A mocked client exception with a particular code.
     */
    protected function getClientException(int $code): ClientException
    {
        $exception = $this->createMock(ClientException::class);
        $reflection = new \ReflectionObject($exception);
        $property = $reflection->getProperty('code');
        $property->setAccessible(true);
        $property->setValue($exception, $code);
        return $exception;
    }
}
