<?php

namespace acromedia\Grovo\tests;

/**
 * @covers \acromedia\Grovo\User
 */
final class UserTest extends GrovoTestBase
{

    /**
     * Ensure our data structures will send the correct data to the API.
     */
    public function testUser()
    {
        $users = $this->getUsers();
        // Expect the contents of the example file.
        $example = file_get_contents(__DIR__ . '/example.ndjson');
        $encoded_users = implode("\n", array_map('json_encode', $users)) . "\n";
        $this->assertSame($example, $encoded_users, 'Encoded users as ndjson.');
    }
}
