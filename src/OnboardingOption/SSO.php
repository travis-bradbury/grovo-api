<?php

namespace acromedia\Grovo\OnboardingOption;

/**
 * Use when user(s) are logging in by Single Sign-on.
 */
class SSO implements OnboardingOptionInterface
{
    public function getOption(): string
    {
        return 'sso';
    }
}
