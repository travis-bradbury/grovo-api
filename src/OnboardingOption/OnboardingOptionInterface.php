<?php

namespace acromedia\Grovo\OnboardingOption;

/**
 * Onboarding types define how new users will log in to Grovo.
 */
interface OnboardingOptionInterface
{
    /**
     * Get the value for this option.
     *
     * @return string
     *   The value of the option expected by the Grovo API.
     */
    public function getOption(): string;
}
