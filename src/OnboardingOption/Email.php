<?php

namespace acromedia\Grovo\OnboardingOption;

/**
 * Use when user(s) will receive a welcome email with a password reset link.
 */
class Email implements OnboardingOptionInterface
{
    public function getOption(): string
    {
        return 'email';
    }
}
