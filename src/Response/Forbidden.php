<?php

namespace acromedia\Grovo\Response;

use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * A response representing a 403 error.
 */
class Forbidden extends BadRequest
{

    /**
     * {@inheritdoc}
     */
    public function __construct(PsrResponseInterface $response)
    {
        parent::__construct($response);
        $this->errors[] = new Error(['title' => 'Forbidden. Did you use the correct API key?']);
    }
}
