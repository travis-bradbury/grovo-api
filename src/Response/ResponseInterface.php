<?php

namespace acromedia\Grovo\Response;

/**
 * A response from Grovo.
 */
interface ResponseInterface
{

    /**
     * Determine if the request was successful.
     *
     * @return bool
     *   True if the request succeeded.
     */
    public function successful(): bool;

    /**
     * Get the errors for a request.
     *
     * @return \acromedia\Grovo\Response\Error[]
     *   A list of errors, empty if none occurred.
     */
    public function getErrors(): array;

    /**
     * Get the actual HTTP response.
     *
     * @return \Psr\Http\Message\ResponseInterface
     *   A PSR 7 response.
     */
    public function getResponse(): \Psr\Http\Message\ResponseInterface;
}
