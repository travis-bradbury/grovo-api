<?php

namespace acromedia\Grovo\Response;

/**
 * A response representing a response with a 4xx HTTP status code.
 */
class BadRequest extends Response
{
}
