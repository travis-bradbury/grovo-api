<?php

namespace acromedia\Grovo\Response;

/**
 * An error processing the request.
 */
class Error
{

    /**
     * The error's message.
     *
     * @var string
     */
    protected $message;

    /**
     * Error constructor.
     *
     * @param $error
     *   An error message from Grovo.
     */
    public function __construct($error)
    {
        $this->message = $error['title'];
    }

    /**
     * Get the error's message.
     *
     * @return string
     *   An error message from Grovo.
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
