<?php

namespace acromedia\Grovo;

/**
 * Represents a user in the Grovo API.
 */
class User implements \JsonSerializable
{

    const ACTIVE = 'active';

    const INACTIVE ='inactive';

    /**
     * A unique identifier for the user.
     *
     * @var string
     */
    public $employeeId;

    /**
     * An associative array of properties for the user.
     *
     * - firstName
     *     The user's first name.
     * - lastName
     *     The user's last name.
     * - primaryEmail
     *     The user's email address.
     * - city
     * - country
     * - department
     * - hireDate
     * - jobTitle
     * - location
     * - region
     * - state
     * - reportsTo
     *     Another user that this one reports to.
     * - status
     *     'active' or 'inactive'.
     * - directRoles
     *     A list of roles for the user.
     *
     * @var array
     */
    protected $data = [];

    /**
     * @param string $employeeId
     *   A unique identifier for the user.
     */
    protected function __construct(string $employeeId)
    {
        $this->employeeId = $employeeId;
    }

    /**
     * Create a user with at least its required fields.
     *
     * @param string $employeeId
     * @param string $primaryEmail
     * @param string $firstName
     * @param string $lastName
     * @return User
     */
    public static function create(string $employeeId, string $primaryEmail, string $firstName, string $lastName): self
    {
        $user = new static($employeeId);
        $user->set('primaryEmail', $primaryEmail);
        $user->set('firstName', $firstName);
        $user->set('lastName', $lastName);
        return $user;
    }

    /**
     * Create a user for doing partial updates, which requires fewer fields.
     *
     * @param string $employeeId
     *
     * @return User
     */
    public static function createPartial(string $employeeId): self
    {
        return new static($employeeId);
    }

    /**
     * Set this user's direct report to another user.
     *
     * @param \acromedia\Grovo\ReportsTo $report
     *   Another user.
     *
     * @return self
     */
    public function setReportsTo(ReportsTo $report): self
    {
        $this->set('reportsTo', $report);
        return $this;
    }

    /**
     * Set a value for a field.
     *
     * @param string $field
     *   The field to set.
     *
     *   See \acromedia\Grovo\User::$data.
     * @param mixed $value
     *   Some value to set.
     *
     * @return self
     */
    public function set(string $field, $value): self
    {
        $this->data[$field] = $value;
        return $this;
    }

    /**
     * Get a value for a field.
     *
     * @param string $field
     *   The name of a field to get the value for.
     * @return mixed
     *   The value of the field or null.
     */
    protected function get(string $field)
    {
        return $this->data[$field] ?? null;
    }

    /**
     * Get all fields.
     *
     * @return array
     *   The value of the field.
     */
    protected function getFields(): array
    {
        return $this->data;
    }

    /**
     * Add a new direct role.
     *
     * @param DirectRole $role
     *   A new role to add.
     *
     * @return self
     */
    public function addDirectRole(DirectRole $role): self
    {
        // I don't understand why this takes more than one line of code.
        $roles = $this->get('directRoles');
        $roles[] = $role;
        $this->set('directRoles', $roles);
        return $this;
    }

    /**
     * Set a user's hire date.
     *
     * @param \acromedia\Grovo\HireDate $date
     *   The user's hire date.
     *
     * @return self
     */
    public function setHireDate(HireDate $date): self
    {
        $this->set('hireDate', $date);
        return $this;
    }

    /**
     * Make the user active.
     *
     * @return self
     */
    public function active(): self
    {
        return $this->set('status', static::ACTIVE);
    }

    /**
     * Make the user inactive.
     *
     * @return User
     */
    public function inactive(): self
    {
        return $this->set('status', static::INACTIVE);
    }

    /**
     * Get the user's status.
     *
     * The value Null indicates that there is no status; the user's status will
     * not be overwritten.
     *
     * @return string|null
     *   'active', 'inactive', or null.
     */
    public function getStatus()
    {
        return $this->get('status');
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
          'employeeId' => $this->employeeId,
          'status' => $this->getStatus(),
        ] + $this->getFields();
    }
}
