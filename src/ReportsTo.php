<?php

namespace acromedia\Grovo;

/**
 * A user reported to by another user.
 */
class ReportsTo implements \JsonSerializable
{

    /**
     * The user being reported to.
     *
     * @var \acromedia\Grovo\User.
     */
    protected $user;

    /**
     * ReportsTo constructor.
     *
     * @param \acromedia\Grovo\User $user
     *   The user being reported to.
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'key' => 'employeeId',
            'value' => $this->user ? $this->user->employeeId : null,
        ];
    }
}
