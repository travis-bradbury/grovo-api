<?php

namespace acromedia\Grovo;

/**
 * A user's hire date.
 */
class HireDate implements \JsonSerializable
{

    /**
     * A user's hire date.
     *
     * @var \DateTime
     */
    protected $date;

    /**
     * HireDate constructor.
     *
     * @param \DateTime $date
     *   Date and time of the hiring.
     */
    public function __construct(\DateTime $date = null)
    {
        $this->date = $date;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->date ? $this->date->format(\DateTime::ATOM) : null;
    }
}
