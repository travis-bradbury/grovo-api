<?php

namespace acromedia\Grovo;

/**
 * A value for a user's direct role.
 */
class DirectRole implements \JsonSerializable
{

    /**
     * The name of the role.
     *
     * @var string
     */
    protected $name;

    /**
     * A direct role for a user.
     *
     * @param string $name
     *   The name of the role.
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return ['key' => 'name', 'value' => $this->name];
    }
}
